<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @see field_preset_preset_value_widget
 */
function field_preset_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  $instance = $form_state['build_info']['args'][0];
  $field = field_info_field($instance['field_name']);

  if (in_array($field['type'], field_presets_supported_field_types())) {
    $form['#submit'][] = 'field_preset_field_edit_form_submit';
    $form['instance']['presets'] = field_preset_preset_value_widget($field, $instance, $form, $form_state);
  }
}

/**
 * Add presets form elements to field settings form.
 */
function field_preset_preset_value_widget($field, $instance, &$form, &$form_state) {
  $field_name = $field['field_name'];

  $element = array(
    '#type' => 'fieldset',
    '#title' => t('Preset value(s)'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#description' => t('Preset values for this field, to make content entry easier.'),
    '#parents' => array('presets'),
    '#prefix' => '<div id="presets-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  $element['add_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add preset'),
    '#submit' => array('field_preset_add_more'),
    '#ajax' => array(
      'callback' => 'field_presets_add_more_callback',
      'wrapper' => 'presets-fieldset-wrapper',
    ),
  );

  if (empty($form_state['num_presets'])) {
    $form_state['num_presets'] = count($instance['presets']) ?: 0;
  }

  for ($i = 0; $i < $form_state['num_presets']; $i++) {
    $element[$i]['preset_name'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Preset Name'),
      '#default_value' => $instance['presets'][$i]['preset_name']
    );

    $element[$i]['remove'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove')
    );

    // Insert the widget.
    $items = $instance['presets'][$i]['preset_value'][LANGUAGE_NONE];
    $instance['required'] = FALSE;
    $instance['description'] = '';

    $element[$i] += field_default_form($instance['entity_type'], NULL, $field, $instance, LANGUAGE_NONE, $items, $element, $form_state, 0);
    $element[$i]['preset_value'] = $element[$i][$field_name];
    unset($element[$i][$field_name]);
  }

  return $element;
}

/**
 * Increment the number of presets available on a field settings page.
 */
function field_preset_add_more($form, &$form_state) {
  $form_state['num_presets']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Return preset form element for AJAX purposes.
 */
function field_presets_add_more_callback($form, &$form_state) {
  return $form['instance']['presets'];
}

/**
 * Form submission handler for field_preset_form_field_ui_field_edit_form_alter().
 */
function field_preset_field_edit_form_submit($form, &$form_state) {
  $instance = $form_state['values']['instance'];
  $field = $form_state['values']['field'];

  // Handle the default value.
  if (isset($form['instance']['presets'])) {
    $element = $form['instance']['presets'];

    $key_exists = NULL;
    // Extract field values.
    $values = drupal_array_get_nested_value($form_state['values'], $element['#parents'], $key_exists);
    if ($key_exists) {
      // Remove the 'value' of the 'add more' button.
      unset($values['add_more']);
      $items = $values;
    }
    // field_default_submit(NULL, NULL, $field, $instance, LANGUAGE_NONE, $items, $element, $form_state);
    foreach($values as $key => $value) {
      if (!$value['remove']) {
        unset($value['remove']);
        $instance['presets'][] = $value;
      }
    }
  }

  // Retrieve the stored instance settings to merge with the incoming values.
  $instance_source = field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
  $instance = array_merge($instance_source, $instance);
  field_update_instance($instance);
}

/**
 * Implements hook_field_widget_form_alter().
 *
 * Add preset selectors at the top of of the form field.
 */
function field_preset_field_widget_form_alter(&$element, &$form_state, &$context) {
  if (isset($element['#field_name']) && isset($context['instance']['presets']) && count($context['instance']['presets']) && $form_state['build_info']['form_id'] != 'field_ui_field_edit_form') {

    $presets     = $context['instance']['presets'];
    $cardinality = $context['field']['cardinality'];
    $name        = $element['#field_name'];
    $id          = drupal_html_id('field-preset-' . $element['#field_name']);

    $element['#prefix'] = '<div id="' . $id . '">';
    $element['#suffix'] = '</div>';

    // Make sure our element validator is first to clean up.
    if (isset($element['#validate'])) {
      $element['#element_validate'] = array_unshift($element['#element_validate'], 'field_preset_field_validate');
    } else {
      $element['#element_validate'] = array('field_preset_field_validate');
    }

    $element['presets'] = array(
      '#type' => 'fieldset',
      '#weight' => -99
    );

    foreach ($presets as $preset) {
      $element['presets'][$preset['preset_name']] = array(
        '#type' => 'button',
        '#value' => filter_xss($preset['preset_name']),
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'callback' => 'field_preset_ajax_callback',
          'wrapper' => $id,
          'effect' => 'fade',
        ),
      );
    }

    if (isset($form_state['values'])) {
      foreach ($presets as $preset) {
        if ($preset['preset_name'] == $form_state['values']['op']) {

          // #default_value will not take unless we clear the input for the field.
          // @see https://www.drupal.org/node/1024962#comment-4021826
          unset($form_state['input'][$name]);

          foreach ($preset['preset_value'][LANGUAGE_NONE] as $delta => $value) {
            if ($cardinality == 1) {
              foreach(array_keys($value) as $key) {
                $element[$key]['#default_value'] = $value[$key];
              }
              break;
            } else {
              $element[$delta]['#default_value'] = $value;
            }
          }

          break;
        }
      }
    }
  }
}

/**
 * Return part of the form that triggered the AJAX callback.
 */
function field_preset_ajax_callback(&$form, &$form_state) {
  return $form[$form_state['triggering_element']['#parents'][0]];
}

/**
 * Remove preset button values from form results before any other validation
 * errors.
 */
function field_preset_field_validate($element, &$form_state, $form) {
  $field_name = $element['#field_name'];
  unset($form_state['values'][$field_name][LANGUAGE_NONE]['presets']);
}

/**
 * @todo  add hook so other modules can add supported field types.
 */
function field_presets_supported_field_types() {
  return array(
    'office_hours',
    'text',
  );
}